// sort.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Constants
#define MAX_NUMBERS (1<<10)

// Function to read in numbers
size_t read_numbers(FILE *stream, int numbers[], size_t n) {
   size_t i = 0;
   while (i < n && scanf("%d", &numbers[i]) != EOF) {
       i++;
   }
   return i;
}

// Comparison function to use for qsort
int cmpnum(const void *a, const void *b) {
   return(*(int *)a > *(int *)b);
}

// Display numbers in array
void disp(int numbers[], size_t n) {
   for (size_t i = 0; i < n; i++) {
      printf("%d\n", numbers[i]);
   }
}
// Main execution
int main(int argc, char *argv[]) {
   // Read numbers into array
   int numbers[MAX_NUMBERS];
   size_t nsize;
   nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
   // Sort numbers and display
   qsort(numbers, nsize, sizeof(int), cmpnum);
   disp(numbers, nsize);
   return EXIT_SUCCESS;
}


