/* moveit.c: Interactive Move Command */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

/* Macros */

#define	streq(a, b) (strcmp(a, b) == 0)
#define strchomp(s) (s)[strlen(s) - 1] = 0

/* Functions */

/**
 * Display usage message and exit.
 * @param   progname    Program name.
 * @param   status      Exit status.
 */
void	usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s files...\n", progname);
    exit(status);
}

/**
 * Save list of file paths to temporary file.
 * @param   files       Array of path strings.
 * @param   n           Number of path strings.
 * @return  Newly allocated path to temporary file.
 */
char *	save_files(char **files, size_t n) {

    // Create temporary file
    char temp[] = "/tmp/fileXXXXXX";
    int fd = mkstemp(temp);
    if (fd < 0) {
        fprintf(stderr, "Unable to create temporary file (%s): %s\n", temp, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Open temporary file into stream
    FILE *stream = fdopen(fd, "w+");
    if (!stream) {
        fprintf(stderr, "Unable to convert file descriptor into stream: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Write file names into file
    for (int i = 0; i < n; i++) {
        fprintf(stream, "%s\n", files[i]);
    }
    fclose(stream);
    char *file = strdup(temp);
    return file;
}

/**
 * Run $EDITOR on specified path.
 * @param   path        Path to file to edit.
 * @return  Whether or not the $EDITOR process terminated successfully.
 */
bool	edit_files(const char *path) {

    // Fork process
    pid_t pid = fork();
    int status = 0;

    // Exit if fork failed
    if (pid < 0) {
        fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    // Transform child process into text editor 
    else if (pid == 0) {
        char *editor = getenv("EDITOR");
        if (!editor) {
            editor = "vim";
        }
        execlp(editor, editor, path, NULL);
        fprintf(stderr, "Execlp failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    // Wait for child and get status
    else {
        while (wait(&status) < 0);
    }
    return WEXITSTATUS(status);
}

/**
 * Rename files as specified in contents of path.
 * @param   files       Array of old path names.
 * @param   n           Number of old path names.
 * @param   path        Path to file with new names.
 * @return  Whether or not all rename operations were successful.
 */
bool	move_files(char **files, size_t n, const char *path) {
    // Open file
    int fd = open(path, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "Unable to open (%s): %s\n", path, strerror(errno));
        return EXIT_FAILURE;
    }

    // Open temporary file into stream
    FILE *stream = fdopen(fd, "r");
    if (!stream) {
        fprintf(stderr, "Unable to convert file descriptor into stream: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    
    // For each file, update name
    char buffer[BUFSIZ];
    char *new = NULL;
    for (int i = 0; i < n; i++) {
        new = fgets(buffer, BUFSIZ, stream);
        // If no more changes, leave names unchanged
        if (!new) {
            return EXIT_SUCCESS;
        }
        // Update if file's name is different
        strchomp(new);
        if (strlen(new) > 1 && !streq(files[i], new)) {
            if (rename(files[i], new) < 0) {
                fprintf(stderr, "Command rename failed: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }
        } 
    }
    
    fclose(stream);
    return EXIT_SUCCESS;
}

/* Main Execution */

int	main(int argc, char *argv[]) {

    // Parse command line arguments
    if (argc == 1) {
        usage(argv[0], EXIT_FAILURE); 
    }
    else if (streq(argv[1], "-h")) {
        usage(argv[0], EXIT_SUCCESS);
    }

    // Save files to temp file
    char *path = save_files(&argv[1], argc - 1);

    // Open text editor
    if (edit_files(path) != 0) {
        unlink(path);
        free(path);
        return EXIT_FAILURE;
    }

    // Update names of files
    if (move_files(&argv[1], argc - 1, path) != 0) {
        unlink(path);
        free(path);
        return EXIT_FAILURE;
    }

    unlink(path);
    free(path);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
