/* moveit.c: Interactive Move Command */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>

/* Macros */

#define	streq(a, b) (strcmp(a, b) == 0)
#define strchomp(s) (s)[strlen(s) - 1] = 0
#define debug(M, ...) \
    if (Verbose) { \
        fprintf(stderr, "%s:%d:%s: " M, __FILE__, __LINE__, __func__, ##__VA_ARGS__); \
    }

/* Globals */

int  Timeout = 10;
bool Verbose = false;

/* Functions */

/**
 * Display usage message and exit.
 * @param   progname    Program name.
 * @param   status      Exit status.
 */
void	usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [options] command...\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -t SECONDS  Timeout duration before killing command (default is %d)\n", Timeout);
    fprintf(stderr, "    -v          Display verbose debugging output\n");
    exit(status);
}

/**
 * Parse command line options.
 * @param   argc        Number of command line arguments.
 * @param   argv        Array of command line argument strings.
 * @return  Array of strings representing command to execute.
 */
char ** parse_options(int argc, char **argv) {

    // Display usage message
    if (argc == 1) {
        usage(argv[0], EXIT_FAILURE);
    }

    // Parse command line arguments
    int argind = 1;
    char *arg = NULL;
    while (argind < argc && (arg = argv[argind])[0] == '-') { 
        argind++;
        switch (arg[1]) {
            case 'h':
                usage(argv[0], EXIT_SUCCESS);
                break;
            case 't':
                arg = argv[argind++];
                Timeout = strtol(arg, NULL, 10); 
                break;
            case 'v':
                Verbose = true;
                break;
            default:
                printf("here\n");
                usage(argv[0], EXIT_FAILURE);
        } 
    }

    debug("Timeout = %d\n", Timeout);
    debug("Verbose = %d\n", (int)Verbose);

    if (argind == argc) {
        usage(argv[0], EXIT_FAILURE);
    }

    // Allocate array and store commands
    char **command = malloc((argc - argind + 1) * sizeof(char *));
    char **temp = NULL;
    char command_str[BUFSIZ] = "";
    for (temp = command; argind < argc; argind++, temp++) {
        *temp = argv[argind];    
        strcat(command_str, argv[argind]);
        strcat(command_str, " ");
    }
    *temp = NULL;

    debug("Command = %s\n", command_str);

    return command;
}

/**
 * Handle signal.
 * @param   signum      Signal number.
 */
void    handle_signal(int signum) {
    debug("Received interrupt: %d\n", signum);
}

/* Main Execution */

int	main(int argc, char *argv[]) {

    // Parse command line arguments
    int status = EXIT_SUCCESS;
    char **command = parse_options(argc, argv);

    // Register signal
    debug("Registering handlers...\n");
    signal(SIGCHLD, handle_signal);

    // Get start time
    debug("Grabbing start time...\n");
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    // Fork process
    pid_t pid = fork();
    // Forking failed
    if (pid < 0) {
       fprintf(stderr, "Failed to fork: %s\n", strerror(errno));
       return EXIT_FAILURE; 
    }
    // Child
    else if (pid == 0) {
        debug("Executing child...\n");
        execvp(command[0], command);
        fprintf(stderr, "%s\n", strerror(errno));
        return EXIT_FAILURE; 

    }
    // Parent
    else {
        int child_status = -1;
       
        // Perform nano sleep
        debug("Sleeping for %d seconds...\n", Timeout);
        struct timespec sleep_time;
        sleep_time.tv_sec = Timeout;
        sleep_time.tv_nsec = 0;
        int sleep_success = nanosleep(&sleep_time, NULL);

        // If signal not received, kill child process
        if (sleep_success != -1 && child_status < 0) {
            debug("Killing child %d\n", pid);
            kill(pid, SIGKILL);
        }

        // Wait for child
        debug("Waiting for child %d\n", pid);
        pid = wait(&child_status);

        // Get exit status of child
        if (WIFEXITED(child_status)) {
            status = WEXITSTATUS(child_status);
        }
        else {
            status = WTERMSIG(child_status);
        }
        debug("Child exit status: %d\n", status);
    }
    
    // Get end time
    debug("Grabbing end time...\n");
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    
    // Print elapsed time
    printf("Time Elapsed: %.1f\n", (float)((end.tv_sec * 1000000000 + end.tv_nsec) - (start.tv_sec * 1000000000 + start.tv_nsec)) / 1000000000);

    free(command);
    return status;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
