Homework 09
===========

1. As discussed in class, working with **system calls** can be tricky because
   unlike most normal functions, they can fail.  For each of the following
   scenarios involving `moveit`, identify which **system calls** would fail (if
   any) and described you handled that situation:

    - No arguments passed to `moveit`.

An if statement checks to see if argc equals one. If there are no arguments passed to `moveit`, then a usage message is displayed with EXIT\_FAILURE status.

    - `$EDITOR` environmental variable is not set.

If the `$EDITOR` environmental variable is not set, then getenv() will fail and return NULL.  An if statement checks to see if NULL is returned and sets the editor to "vim" if the command failed.

    - User has run out of processes.
 
If the user has run out of processes, then the fork() command will fail and return a pid of -1.  If the returned pid is less than one, an error statement is printed, and the program exits with a status of EXIT\_FAILURE.

    - `$EDITOR` program is not found.

If the `$EDITOR` program is not found, then execlp() will failed.  Upon failure, the child process will not be transformed, and it will execute the following lines which print an error statement and exit with an EXIT\_FAILURE exit status.

    - Temporary file is deleted before moving files.

If the temporary file is deleted before moving the files, then the open command in the move\_files function will fail and return a file descriptor of -1.  An if statement checks if the file descriptor is less than 0, then prints an error statement and returns with EXIT\_FAILURE status.

    - Destination path is not writable.

If the destination path is not writable, then the command rename() will fail and return a value of -1.  If the result is less than 0, then an error message is printed and the program exits with EXIT\_FAILURE.

2. As described in the project write-up, the **parent** is doing most of the
   work in `timeit` since it `forks`, `times`, and `waits` for the **child**
   (and possibly kills it), while the child simply calls `exec`.  To distribute
   the work more evenly, **Bill** proposes the following change:

    > Have the **child** set an `alarm` that goes off after the specified
    > `timeout`.  In the signal handler for the `alarm`, simply call `exit`, to
    > terminate the **child**.  This way, the **parent** just needs to `wait`
    > and doesn't need to perform a `kill` (since the **child** will terminate
    > itself after the `timeout`).

    Is this a good idea?  Explain why or why not.

This approach would not work because the exec() command transforms the child process entirely. Specifically, it overwrites the original code segment and reinitializes all of the other parts of the memory space. As a result, the `alarm` could not trigger the signal handler to call `exit`; the child process no longer contains the previously included code.
