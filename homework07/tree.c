/* tree.c: Huffman Tree */

#include "queue.h"
#include "tree.h"

#include <string.h>

/* Internal Huffman Tree Prototyes */

void	tree_count(Tree *t, FILE *stream);
Queue *	tree_queue(Tree *t);
void	tree_walk_node(const Node *n, NodeFunc f, void *arg, char *encoding);

/* External Huffman Tree Functions */

/**
 * Create Tree structure.
 * @return  Newly allocated and initialized Tree structure.
 */
Tree *  tree_create() {
    Tree *t = malloc(sizeof(Tree));
    if (t) {
        t->root = NULL;
        for (int i = 0; i < TREE_COUNTS_SIZE; i++) {
            t->counts[i] = 0;
        }
    }
    return t;
}

/**
 * Delete Tree structure.
 * @param   t           Tree structure.
 * @return  NULL.
 */
Tree *  tree_delete(Tree *t) {
    node_delete(t->root);
    free(t);
    return NULL;
}

/**
 * Build internal Tree structure.
 * @param   t           Tree structure.
 * @param   stream      I/O stream to read from.
 * @return  Whether or not the operation succeeded.
 */
bool	tree_build(Tree *t, FILE *stream) {
    // Construct frequency table
    tree_count(t, stream);
    // Construct priority queue
    Queue *q = tree_queue(t);
    Node *n1 = NULL;
    Node *n2 = NULL;
    // Process the priority queue to build tree
    while (q->size > 1) {
        n1 = queue_pop(q);
        n2 = queue_pop(q);
        queue_push(q, node_create(NODE_NOT_SYMBOL, n1->count + n2->count, n1, n2));
    }
    t->root = queue_pop(q);
    queue_delete(q);
    return t;
}

/**
 * Walk Tree structure.
 * @param   t           Tree structure.
 * @param   f           Node function.
 * @param   arg         Ndoe function argument.
 * @return  Whether or not the operation succeeded.
 */
void	tree_walk(const Tree *t, NodeFunc f, void *arg) {
    char encoding[BUFSIZ] = "";
    tree_walk_node(t->root, f, arg, encoding);
}

/* Internal Tree Functions */

/**
 * Build internal counts table in Tree structure.
 * @param   t           Tree structure.
 * @param   stream      I/O stream to read from.
 * @return
 */
void	tree_count(Tree *t, FILE *stream) {
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stream)) {
        char *c = buffer;
        while (*c) {
            t->counts[(int)(*c)]++;
            c++;
        }
    }
}

/**
 * Create Priority Queue out of counts table in Tree structure.
 * @param   t           Tree structure.
 * @return  Priority Queue of Nodes corresponding to counts in Tree.
 */
Queue *	tree_queue(Tree *t) {
    Queue *q = queue_create(0); 
    for (int i = 0; i < TREE_COUNTS_SIZE; i++) {
        size_t freq = t->counts[i];
        if (freq != 0) {
            queue_push(q, node_create(i, freq, NULL, NULL));
        }
    }
    return q;
}

/**
 * Recursively walk Tree nodes.
 * @param   n           Node structure.
 * @param   f           Node function.
 * @param   arg         Node function argument.
 * @param   encoding    Node encoding.
 */
void	tree_walk_node(const Node *n, NodeFunc f, void *arg, char *encoding) {
    // Base case for nullptr
    if (n == NULL) {
        return;
    }
    // If tree only contains one node
    if (strcmp(encoding, "") == 0 && n->left == NULL && n->right == NULL) {
        strcat(encoding, "0");
    }
    // Perform function
    if (n->symbol.letter != NODE_NOT_SYMBOL) {
        f(n, encoding, arg);
    }
    // Make copy of encoding
    char tempLeft[BUFSIZ] = "";
    char tempRight[BUFSIZ] = "";
    strcpy(tempLeft, encoding);
    strcpy(tempRight, encoding);
    // Recursive calls to left and right node
    tree_walk_node(n->left, f, arg, strcat(tempLeft, "0"));
    tree_walk_node(n->right, f, arg, strcat(tempRight, "1"));
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
