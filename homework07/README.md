Homework 07
===========

# Activity 2 - Questions

1. Describe how the `symbol` is stored in the `Node` **struct** by diagramming
   the memory layout of the following `Node` structure:

        Node *n = node_create('\n', 2, NULL, NULL);

    Be sure to indicate how much memory each `Node` structure occupies and how
    much is allocated for each attribute in the struct.
    
The declaration of a Node \* "n" allocates memory on the heap for the address of a node structure. This stores an 8 byte address which points to a Node struct. The function "node\_create" allocates memory on the heap for the size of the Node struct, which is determined by its four attributes. Specifically, the size\_t "count" requires 8 bytes, and the two Node pointers ("left" and "right") each require 8 bytes. The Symbol union which stores either the int64\_t "letter" or a char array "string" the size of an int64\_t also only requires 8 bytes. Altogether, the Node struct requires 32 bytes of allocated memory on the heap.

    1. Explain what the code `assert(n->string.letter == '\n')` would result in?

Because the function node\_set\_symbol escapes the char '\n' upon insertion, the assertion above would always fail. The characters which require escaping are stored as a string instead of an int. Each char in the string occupies a byte in memory. Because n->symbol.letter only reads the first char of the string, it will compare the char '\' ASCII value to that of '\n'. Because these are different, the test will fail.
    
    2. Given that `symbol` is a **union** type, is it ever possible for both the
      `letter` and `string` of the symbol `\n` to co-exist (that is, the
      `string` doesn't clobber the `letter`)?
      
        **Hint**: Consider alternative architectures.

When little endian byte order is used, the least significant byte is stored in the lowest memory location. The n->symbol.letter is therefore stored at the lowest address. When a string is created instead, it starts at the lowest memory location as well, overwriting the char value with the new string. Therefore, the two cannot co-exist in a little endian machine. However, big endian byte order stores the most significant byte in the lowest memory location. This means the char value would be stored in the highest location in memory while the string would still begin at the lowest memory location. Because a char is only 1 bytes and the strings used are all less than 7 bytes, these two can con-exist in the 8 byte memory location allocated for the symbol.

2. To construct the Huffman code, you had to utilize both a **priority queue**
   (aka. **min-heap**) and a **Huffman Tree**, both of which were binary trees.
   For the former, you used an **array** representation while for the later you
   used a **linked node** representation.  With this in mind, answer the
   following questions:

    1. Would it have been easier to implement the **priority queue** with a
      **linked node** representation?  Explain why this would be better or
      worse than the **array** representation.

It would be more difficult to implement the priority queue with a linked node representation because insertion must be made in the next available space to maintain a complete binary tree. Finding the place to insert would require a breadth first traversal of the tree with O(n) complexity each time. With an array, the size of the queue can be used to easily access the next open position with O(1) complexity. The array makes it easy to maintain a complete tree. Furthermore, the bubble up function would require doubly linked nodes in order to access the parent of each element. The array representation has index operations which makes it much easier to find the parent node. Note, array representations also store the nodes contiguously in memory which offers better cache locality than linked nodes.

    2. Would it have been easier to implement the **Huffman Tree** with an 
      **array** representation?  Explain why this would be better or worse 
      than the **linked node** representation.

Using an array representation for the Huffman Tree would be more difficult. Nodes need to be inserted at the root of the tree, which can be inserted at the beginning of a list with O(1) complexity by simply pointing the "left" and "right" pointers to the appropriate nodes. Inserting at the root using an array would require shifting all of the other nodes, making insertion O(n). Traversing the list to find the encoding would be more difficult because it would require the use of index functions to find the children rather than using Node pointers. Also, because the Huffman Tree is not complete, an array would contain many empty spaces, using far more memory than the Tree actually requires.

3. Analyze the results of your experiements with your `huff.py` Python script.
   What sort of compression ratios were you able to achieve?  How does this
   compare to standard utilities such as `gzip`, `bzip2`, and `xz`?

    |           FILE           | OLD SIZE | NEW SIZE |   RATIO   |
    |--------------------------|----------|----------|-----------|
    |   pride_and_prejudice.txt|   5712504|   3238750|     56.70%|
    |          frankenstein.txt|   3579728|   2005480|     56.02%|
    |     heart_of_darkness.txt|   1866304|   1065323|     57.08%|
    |    tale_of_two_cities.txt|   6298280|   3575846|     56.77%|
    |   alice_in_wonderland.txt|   1316256|    762209|     57.91%|
Note: File sizes above are displayed in bits.

alice\_in\_wonderland.txt (BYTES)

    |  FILE TYPE |  SIZE  |  RATIO  |
    |------------|--------|---------|
    |    original|  164532|  100.00%|
    |        huff|   95276|   57.91%|
    |        gzip|   59394|   36.10%| 
    |       bzip2|   48151|   29.27%| 
    |          xz|   52868|   32.13%| 
Note: File sizes above are displayed in bytes.

The Huffman Utility compressed the files at approximately 55% to 60% of the original size. Compared to standard utilities, the files compressed using huffman encodings was about double the size, making it a far less effective compression tool. However, it did significantly impact the size of the file.

