#!/usr/bin/env python3

import os
import yaml

# Make list of books and find current path
books = os.listdir('data')
path = os.getcwd() + '/data/'

# Print header
print("|           FILE           | OLD SIZE | NEW SIZE |   RATIO   |")
print("|--------------------------|----------|----------|-----------|")

# Create each row in table
for book in books:
   # Create dictionary with huffman encoding
   huff = yaml.load(os.popen("./huff -f yaml {}{}".format(path, book)))
   # Find old and new size
   oldSize = os.stat(path + book).st_size * 8
   newSize = 0 
   for char in huff:
      newSize += (len(huff[char]['encoding']) * huff[char]['count'])
   ratio = newSize / oldSize * 100
   # Print row
   print('|{:>26}|{:>10}|{:>10}|{:>10.2f}%|'.format(book, oldSize, newSize, ratio))
