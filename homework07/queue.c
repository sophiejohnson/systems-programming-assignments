/* queue.c: Priority Queue (Min Heap) */

#include "debug.h"
#include "queue.h"

/* Internal Priority Queue Prototypes */

bool    queue_resize(Queue *q, size_t capacity);
void    queue_bubble_up(Queue *q, size_t i);
void    queue_bubble_down(Queue *q, size_t i);
size_t  queue_minimum_child(Queue *q, size_t i);

/* External Priority Queue Functions */

/**
 * Create Queue structure.
 * @param   capacity        Initial capacity.
 * @return  Newly allocated Queue structure with specified capacity.
 */
Queue * queue_create(size_t capacity) {
    Queue *q = calloc(1, sizeof(Queue));
    if (q) {
        q->size = 0;
        q->capacity = (capacity == 0)? PRIORITY_QUEUE_CAPACITY : capacity;
        q->nodes = calloc(q->capacity, sizeof(Node *));
        if (q->nodes == NULL) {
            free(q);
            return NULL;
        }
    }
    return q;
}

/**
 * Delete Queue structure.
 * @param   q           Queue structure.
 * @return  NULL.
 */
Queue * queue_delete(Queue *q) {
    free(q->nodes);
    free(q);
    return NULL;
}

/**
 * Push Node into Queue structure.
 * @param   q           Queue structure.
 * @param   n           Node structure.
 * @return  Whether or not the operation was successful.
 */
bool    queue_push(Queue *q, Node *n) {
    // Resize if necessary
    if (q->size == q->capacity) {
        queue_resize(q, q->capacity);
    }
    // Insert in array and bubble up
    q->nodes[q->size] = n;
    queue_bubble_up(q, q->size);
    q->size++;
    return q;
}

/**
 * Pop minimum Node into Queue structure.
 * @param   q           Queue structure.
 * @return  Minimum Node structure (by count) or NULL.
 */
Node *	queue_pop(Queue *q) {
    Node *min = NULL;
    if (q->size > 0) {
        // Record minimum node
        min = q->nodes[0];
        // Move last node to top of queue and bubble down
        q->nodes[0] = q->nodes[q->size - 1];
        q->nodes[q->size - 1] = NULL;
        q->size--;
        queue_bubble_down(q, 0);
    }
    return min;
}

/**
 * Dump Queue structure.
 * @param   q           Queue structure.
 * @param   stream      I/O stream to write to.
 */
void    queue_dump(const Queue *q, FILE *stream) {
    for (size_t i = 0; q->nodes[i] != NULL && i < q->capacity; i++) {
        node_dump(q->nodes[i], stream);
    }
}

/* Internal Priority Queue Functions */

/**
 * Resize Queue structure.
 * @param   q           Queue structure.
 * @param   capacity    New capacity.
 * @return  Whether or not operation succeeded.
 */
bool    queue_resize(Queue *q, size_t capacity) {
    // Reallocate with new capacity
    size_t newCapacity = 2 * capacity;
    q->nodes = realloc(q->nodes, newCapacity * sizeof(Node *));
    // Initialize values to 0
    for (int i = capacity; i != newCapacity; i++) {
        q->nodes[i] = NULL;
    }
    // Record new capacity
    q->capacity = newCapacity;
    return q;
}

/**
 * Bubble up Node in Queue structure.
 * @param   q           Queue structure.
 * @param   i           Index to current Node structure.
 */
void    queue_bubble_up(Queue *q, size_t i) {
    if (i != 0) {
        size_t parent = PARENT(i);
        Node *temp = NULL;
        do {
            // If node's count is less than parent's, swap
            if ((q->nodes[i])->count < (q->nodes[parent])->count) {
                temp = q->nodes[i];
                q->nodes[i] = q->nodes[parent];
                q->nodes[parent] = temp;
            }
            // Check next parent
            i = parent;
            parent = PARENT(i);
        } while (i > 0);
    }
}

/**
 * Bubble down Node in Queue structure.
 * @param   q           Queue structure.
 * @param   i           Index to current Node structure.
 */
void    queue_bubble_down(Queue *q, size_t i) {
    size_t left = LEFT_CHILD(i);
    size_t right = RIGHT_CHILD(i);
    size_t minChild = 0;
    Node *temp = NULL;
    // Continue to find smallest child as long as children exist
    while (left < q->size || right < q->size) {
        minChild = queue_minimum_child(q, i);
        // Swap if node's count is greater than min child's
        if ((q->nodes[i])->count > (q->nodes[minChild])->count) {
            temp = q->nodes[i];
            q->nodes[i] = q->nodes[minChild];
            q->nodes[minChild] = temp;
        }
        i = minChild;
        left = LEFT_CHILD(i);
        right = RIGHT_CHILD(i);
    }
}

/**
 * Determines index of child with minimum value.
 * @param   q           Queue structure.
 * @param   i           Index to current Node structure.
 */
size_t  queue_minimum_child(Queue *q, size_t i) {
    size_t left = LEFT_CHILD(i);
    size_t right = RIGHT_CHILD(i);
    size_t minChild = 0;
    // Find smallest child
    if (left >= q->size) {
        minChild = right;
    }
    else if (right >= q->size) {
        minChild = left;
    }
    else {
        minChild = ((q->nodes[left])->count < (q->nodes[right])->count)? left : right; 
    }
    return minChild;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
