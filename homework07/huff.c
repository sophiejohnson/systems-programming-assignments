/* huff.c */

#include "tree.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants */

typedef enum {
    TSV,
    CSV,
    YAML,
    JSON,
} Format;

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s FILES...\n", PROGRAM_NAME);
    fprintf(stderr, "Flags:\n\n");
    fprintf(stderr, "    -f FORMAT    Output format (tsv, csv, yaml, json)\n");

    exit(status);
}

// Set enum to correct format
void set_format(Format *f, char *str) {
    if (strncmp(str, "tsv", strlen(str)) == 0) {
        *f = TSV;
    }
    else if (strncmp(str, "csv", strlen(str)) == 0) {
        *f = CSV;
    }
    else if (strncmp(str, "yaml", strlen(str)) == 0) {
        *f = YAML;
    }
    else if (strncmp(str, "json", strlen(str)) == 0) {
        *f = JSON;
    }
}

// Read file to build tree
void read_file(Tree *t, const char *path) {
    FILE *fs = fopen(path, "r");
    if (fs == NULL) {
        fprintf(stderr, "%s: %s: %s\n", PROGRAM_NAME, path, strerror(errno));
        return;
    }
    tree_build(t, fs);
    fclose(fs);
}

// Display contents of node with appropriate formatting
void disp_node(const Node *n, char *encoding, void *arg) {
    switch (*(Format *)arg) {
        case TSV:
            printf("%s\t%s\t%d\n", n->symbol.string, encoding, (int)n->count);
            break;
        case CSV:
            printf("\"%s\",\"%s\",%d\n", n->symbol.string, encoding, (int)n->count);
            break;
        case YAML:
            printf("\"%s\": {\"encoding\": \"%s\", \"count\": %d}\n", n->symbol.string, encoding, (int)n->count);
            break;
        case JSON:
            printf("  \"%s\": {\"encoding\": \"%s\", \"count\": %d},\n", n->symbol.string, encoding, (int)n->count);
            break;
    }
}

// Display by walking through tree with display node function
void disp_huff(Tree *t, void *arg) {
    if (*(Format *)arg == JSON) {
        printf("{\n");
    }
    tree_walk(t, disp_node, arg);
    if (*(Format *)arg == JSON) {
        printf("}\n");
    }
}


/* Main Execution */

int main(int argc, char *argv[]) {
    // Parse command line arguments
    int argind = 1;
    PROGRAM_NAME = argv[0];
    Format format = TSV;
    while (argind < argc && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 'f':
                // Set format
                set_format(&format, argv[argind++]);      
                break;
            default:
                usage(1);
                break;
        }
    }
    // Make huff tree depending on input
    Tree *t = tree_create();
    // Read stdin
    if (argind == argc) {
        tree_build(t, stdin);
    }
    // Process file
    else {
        while (argind < argc) {
            char *path = argv[argind++];
            read_file(t, path);
        }
    }
    // Display encoding with appropriate formatting
    disp_huff(t, &format); 
    tree_delete(t);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
