#!/usr/bin/env python3

import sys

print(' '.join(list(filter(lambda x: int(x) % 2 == 0, sys.stdin.read().splitlines()))))
