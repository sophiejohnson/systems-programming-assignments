#!/bin/sh

#!/bin/sh

# Check if there are no arguments
if [ "$#" -lt 1 ]; then
       echo "Usage: exists.sh file0..."
       exit 1
fi

# Check each command line argument
EXITCODE=0
for arg in "$@"; do
    # Check if argument exists
    if [ -e "$arg" ]; then
        echo "$arg exists!"
    else
        echo "$arg does not exist!"
        EXITCODE=1
    fi
done

exit $EXITCODE
