#!/usr/bin/env python3

import sys
import os

exitcode = 0

# Usage message if no command line arguments
if len(sys.argv) == 1:
   print("Usage: exists.py file0...")
   exitcode = 1

# Check existence of each file name 
for arg in sys.argv[1:]:
   file = os.getcwd() + '/' + arg
   if os.path.isfile(file):
      print(arg + " exists!")
   else:
      print(arg + " does not exist!")
      exitcode = 1

# Exit with correct exit code
sys.exit(exitcode)
