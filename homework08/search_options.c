/* search_options.c: Options */

#include "search.h"

#include <unistd.h>

/* Options Functions */

/**
 * Display usage message and exit.
 * @param   progname        Name of program.
 * @param   status          Exit status.
 */
void        options_usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s PATH [OPTIONS]\n", progname);
    fprintf(stderr, "\nOptions:\n");
    fprintf(stderr, "    -help           Display this help message\n\n");
    fprintf(stderr, "    -executable     File is executable or directory is searchable to user\n");
    fprintf(stderr, "    -readable       File readable to user\n");
    fprintf(stderr, "    -writable       File is writable to user\n\n");
    fprintf(stderr, "    -type [f|d]     File is of type f for regular file or d for directory\n\n");
    fprintf(stderr, "    -empty          File or directory is empty\n\n");
    fprintf(stderr, "    -name  PATTERN  Base of file name matches shell PATTERN\n");
    fprintf(stderr, "    -path  PATTERN  Path of file matches shell PATTERN\n\n");
    fprintf(stderr, "    -perm  MODE     File's permission bits are exactly MODE (octal)\n");
    fprintf(stderr, "    -newer fILE     File was modified more recently than FILE\n\n");
    fprintf(stderr, "    -uid   N        File's numeric user ID is N\n");
    fprintf(stderr, "    -gid   N        File's numeric group ID is N\n");
    exit(status);
}

/**
 * Parse command-line options.
 * @param   argc            Number of command-line arguments.
 * @param   argv            Array of command-line arguments.
 * @param   root            Pointer to root string.
 * @param   options         Pointer to Options structure.
 * @return  Whether or not parsing the command-line options was successful.
 */
bool        options_parse(int argc, char **argv, char **root, Options *options) {
    if (argc != 1) {
        // Record root directory
        int argind = 1;
        if (argv[1][0] != '-') {
            *root = argv[argind++];
        }
        // Parse rest of command line arguments 
        char *arg;
        while (argind < argc && (arg = argv[argind++])[0] == '-') {
            arg++;
            if (streq(arg, "help")) {
                options_usage(argv[0], 0);
            } 
            // Access mode
            else if (streq(arg, "executable")) {
                options->access |= X_OK;
            }
            else if (streq(arg, "readable")) {
                options->access |= R_OK;
            }
            else if (streq(arg, "writable")) {
                options->access |= W_OK;
            } 
            // Type (file or dir)
            else if (streq(arg, "type")) {
                arg = argv[argind++];
                if (streq(arg, "f")) {
                    options->type = -1;
                }
                else if (streq(arg, "d")) {
                    options->type = 1; 
                } 
            }
            // Empty file/dir
            else if (streq(arg, "empty")) {
                options->empty = true;
            }
            // Regular expressions for path or name
            else if (streq(arg, "name")) {
                arg = argv[argind++];
                options->name = arg;
            } 
            else if (streq(arg, "path")) {
                arg = argv[argind++];
                options->path = arg;
            }
            // Permissions
            else if (streq(arg, "perm")) {
                arg = argv[argind++];
                options->perm = strtol(arg, NULL, 8);
            }
            // Newer than 
            else if (streq(arg, "newer")) {
                arg = argv[argind++];
                options->newer = get_mtime(arg);
            }
            // User and group id numbers
            else if (streq(arg, "uid")) {
                arg = argv[argind++];
                options->uid = strtol(arg, NULL, 10);
            }
            else if (streq(arg, "gid")) {
                 arg = argv[argind++];
                options->gid = strtol(arg, NULL, 10);
            }
            else {
                options_usage(argv[0], 1);
            }
        }
        // If invalid arguments, usage message
        if (argind < argc) {
            options_usage(argv[0], 1);
        }
    } 
    return EXIT_SUCCESS; 
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
