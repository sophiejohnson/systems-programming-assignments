/* search_main.c: Main Execution */

#include "unistd.h"

#include "search.h"

/* Main Execution */

int main(int argc, char *argv[]) {

    /* Parse options */
    char *root = "."; 
    Options options = {0, 0, false, NULL, NULL, 0, 0, -1, -1};
    options_parse(argc, argv, &root, &options);

    /* Check root */
    if (!filter(root, &options)) {
        printf("%s\n", root);
    }

    /* Walk root */
    walk(root, &options);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
