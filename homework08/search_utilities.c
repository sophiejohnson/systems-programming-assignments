/* search_utilities.c: Utilities */

#include "search.h"

#include <errno.h>
#include <dirent.h>

/* Utility Functions */

/**
 * Checks whether or not the directory is empty.
 * @param   path    Path to directory.
 * @return  Whether or not the path is an empty directory.
 */
bool        is_directory_empty(const char *path) {

    // Open directory
    DIR *dir = opendir(path);
    if (dir == NULL) {
        fprintf(stderr, "Unable to open: %s", strerror(errno));
        return false;
    }
    
    // Check number of entries
    struct dirent *entry;
    int i = 0;
    while ((entry = readdir(dir)) != NULL && i < 3) {
        i++;
    }
    closedir(dir);

    // If empty, return true
    if (i == 2) {
        return true;
    }
    
    return false;
}

/**
 * Retrieves modification time of file.
 * @param   path    Path to file.
 * @return  The modification time of the file.
 */
time_t      get_mtime(const char *path) {
    struct stat stat;
    int status = lstat(path, &stat);
    if (status < 0) {
        fprintf(stderr, "lstat: `%s: %s\n", path, strerror(errno));
        return EXIT_FAILURE;
    }
    return stat.st_mtime;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
