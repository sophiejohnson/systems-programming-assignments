/* search_filter.c: Filters */

#include "search.h"

#include <errno.h>
#include <string.h>

#include <fnmatch.h>
#include <unistd.h>

/* Internal Filter Functions */

// Check access mode 
bool        filter_access(const char *path, const struct stat *stat, const Options *options) {
    return options->access && access(path, options->access) != 0;
}

// Check if file or directory 
bool        filter_type(const char *path, const struct stat *stat, const Options *options) {
    return (options->type && !((options->type < 0)? S_ISREG(stat->st_mode) : S_ISDIR(stat->st_mode)));
}

// Check if empty 
bool        filter_empty(const char *path, const struct stat *stat, const Options *options) {
    bool isEmpty = true;
    if (options->empty) {
        // Check directory
        if (S_ISDIR(stat->st_mode)) {
            isEmpty = !is_directory_empty(path);
        }
        // Check file
        else if (S_ISREG(stat->st_mode)) {
            isEmpty = !(stat->st_size == 0);
        }
        return isEmpty;
    }
    return !isEmpty;
}

// Check if name contains regular expression 
bool        filter_name(const char *path, const struct stat *stat, const Options *options) {
    char *file = strrchr(path, '/');
    file++;
    return options->name != NULL && fnmatch(options->name, file, 0) != 0;
}

// Check if path contains regular expression 
bool        filter_path(const char *path, const struct stat *stat, const Options *options) {
    return options->path != NULL && fnmatch(options->path, path, 0) != 0;
}

// Check permissions
bool        filter_perm(const char *path, const struct stat *stat, const Options *options) {
    return options->perm && ((stat->st_mode & (S_IRWXU | S_IRWXG | S_IRWXO)) != options->perm);
}

// Check if newer than time entered  
bool        filter_newer(const char *path, const struct stat *stat, const Options *options) {
    return options->newer && options->newer >= stat->st_mtime;
}

// Check uid
bool        filter_uid(const char *path, const struct stat *stat, const Options *options) {
    return options->uid >= 0 && options->uid != stat->st_uid;
}

// Check gid
bool        filter_gid(const char *path, const struct stat *stat, const Options *options) {
    return options->gid >= 0 && options->gid != stat->st_gid;
}

/* Array of function pointers. */
FilterFunc FILTER_FUNCTIONS[] = {      
    filter_access,
    filter_type,
    filter_empty,
    filter_name,
    filter_path,
    filter_perm,
    filter_newer,
    filter_uid,
    filter_gid,
    NULL
};

/* External Filter Functions */

/**
 * Filter path based options.
 * @param   path        Path to file to filter.
 * @param   options     Pointer to Options structure.
 * @return  Whether or not the path should be filtered out (false means include
 * it in output, true means exclude it from output).
 */
bool	    filter(const char *path, const Options *options) {
    // Find stat of path
    struct stat stat;
    int status = lstat(path, &stat);
    if (status < 0) {
        fprintf(stderr, "lstat: `%s: %s\n", path, strerror(errno));
        return EXIT_FAILURE;
    }
    
    // Check each filter in array
    int i = 0;
    while (FILTER_FUNCTIONS[i] != NULL) {
        // If filter is true, return true
        if (FILTER_FUNCTIONS[i](path, &stat, options)) {
            return true;    
        }
        i++;
    }
    
    // If no filter is true, return false
    return false;
}
/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
