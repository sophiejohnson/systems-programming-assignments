/* duplicate_main.c: Main Execution */

#include "duplicate.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>

/* Main Execution */

int         main(int argc, char *argv[]) {

    // Parse command line arguments to create struct
    Options opts = {"stdin", "stdout", INT_MAX, DEFAULT_BYTES, 0, 0};
    parse_options(argc, argv, &opts);
   
    // Open stdin or input file
    int input_fd = 0;
    if (!streq(opts.input_file, "stdin")) {
        input_fd = open(opts.input_file, O_RDONLY);
        if (input_fd < 0) {
            fprintf(stderr, "Unable to open (%s): %s\n", opts.input_file, strerror(errno));
            return EXIT_FAILURE; 
        }
    }
    // Open stdout or output file
    int output_fd = 1;
    if (!streq(opts.output_file, "stdout")) {
        output_fd = open(opts.output_file, O_WRONLY | O_CREAT, 0666);
        if (output_fd < 0) {
            fprintf(stderr, "Unable to open (%s): %s\n", opts.input_file, strerror(errno));
            return EXIT_FAILURE;
        }
    }
        
    // Allocate buffer with appropriate block size 
    // char buffer[opts.bytes];
    char *buffer = calloc(opts.bytes, sizeof(char));
    if (!buffer) {
        return EXIT_FAILURE;
    }

    // Skip (number of blocks to skip in input file) 
    if (opts.skip > 0) {
        int off_in = lseek(input_fd, opts.skip * opts.bytes, SEEK_SET);
        if (off_in < 0) {
            fprintf(stderr, "Unable to lseek %d blocks (%s): %s\n", (int)opts.skip, opts.input_file, strerror(errno));
            return EXIT_FAILURE;
        }
    }

    // Seek (number of blocks to skip in output file)
    if (opts.seek > 0) {
        int off_out = lseek(output_fd, opts.seek * opts.bytes, SEEK_SET);
        if (off_out < 0) {
            fprintf(stderr, "Unable to lseek %d blocks (%s): %s\n", (int)opts.seek, opts.output_file, strerror(errno));
            return EXIT_FAILURE;
        }
    }

    // Copy data into output file or stdout
    int data_rd = 0, blocks = 0;
    while((data_rd = read(input_fd, buffer, opts.bytes)) > 0 && blocks < opts.count) {
        int data_wrote = 0, data_written = 0;
        do {
            data_wrote = write(output_fd, buffer + data_written, data_rd - data_written);
            if(data_wrote < 0) {
                fprintf(stderr, "Unable to write (%s): %s\n", opts.output_file, strerror(errno));
                return EXIT_FAILURE;
            }
            data_written += data_wrote;
        } while(data_written != data_rd);
        blocks++;
    } 
    if (data_rd < 0) {
        fprintf(stderr, "Unable to read (%s): %s\n", opts.output_file, strerror(errno));
        return EXIT_FAILURE;
    }

    // Clean up allocated or requested resources
    close(input_fd);
    close(output_fd);
    free(buffer);
    
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
