/* search_walk.c: Walk */

#include "search.h"

#include <errno.h>
#include <dirent.h>

/* Walk Functions */

/**
 * Recursively walk the root directory with specified options.
 * @param   root        Root directory path.
 * @param   options     User specified filter options.
 * @return  Whether or not walking this directory was successful.
 */
int         walk(const char *root, const Options *options) {
    
    // Open directory
    DIR *dir = opendir(root);
    if (dir == NULL) {
        fprintf(stderr, "search: `%s: %s\n", root, strerror(errno));
        return EXIT_FAILURE;
    } 
    
    // Check each entry in the directory
    struct dirent *entry = NULL;
    char path[BUFSIZ];
    while ((entry = readdir(dir)) != NULL) {
        
        // Filter each entry
        if (!streq(entry->d_name, ".") && !streq(entry->d_name, "..")) {
            snprintf(path, BUFSIZ, "%s/%s", root, entry->d_name);
            if (!filter(path, options)) {
                printf("%s\n", path);
            }

            // Recursive call on directories
            if (entry->d_type == DT_DIR) {
                walk(path, options);
            }
        }
    }

    // Close directory
    closedir(dir);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
