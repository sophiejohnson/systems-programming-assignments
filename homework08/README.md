Homework 08
===========

## Strace Output 

### `duplicate`

strace -c ./duplicate if=/etc/passwd > /dev/null
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
100.00    0.000012           2         7           write
  0.00    0.000000           0         9           read
  0.00    0.000000           0        27        24 open
  0.00    0.000000           0         4           close
  0.00    0.000000           0        24        18 stat
  0.00    0.000000           0         2           fstat
  0.00    0.000000           0         8           mmap
  0.00    0.000000           0         3           mprotect
  0.00    0.000000           0         1           munmap
  0.00    0.000000           0         3           brk
  0.00    0.000000           0         1         1 access
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         1           arch_prctl
------ ----------- ----------- --------- --------- ----------------
100.00    0.000012                    91        43 total

### `dd`
strace -c dd if=/etc/passwd > /dev/null
6+1 records in
6+1 records out
3148 bytes (3.1 kB) copied, 0.000529552 s, 5.9 MB/s
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
 65.85    0.000027           1        24        18 stat
 34.15    0.000014           1        17           mmap
  0.00    0.000000           0        13           read
  0.00    0.000000           0        10           write
  0.00    0.000000           0        49        41 open
  0.00    0.000000           0        11           close
  0.00    0.000000           0         7           fstat
  0.00    0.000000           0         1           lseek
  0.00    0.000000           0         7           mprotect
  0.00    0.000000           0         2           munmap
  0.00    0.000000           0         3           brk
  0.00    0.000000           0         6           rt_sigaction
  0.00    0.000000           0         1           rt_sigprocmask
  0.00    0.000000           0         1         1 access
  0.00    0.000000           0         1           dup2
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         1           getrlimit
  0.00    0.000000           0         1           arch_prctl
  0.00    0.000000           0         2         1 futex
  0.00    0.000000           0         1           set_tid_address
  0.00    0.000000           0         1           set_robust_list
------ ----------- ----------- --------- --------- ----------------
100.00    0.000041                   160        61 total

### `search`
strace -c ./search /etc/passwd > /dev/null
search: `/etc/passwd: Not a directory
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
100.00    0.000013           7         2           write
  0.00    0.000000           0         1           read
  0.00    0.000000           0        27        25 open
  0.00    0.000000           0         2           close
  0.00    0.000000           0        24        18 stat
  0.00    0.000000           0         3           fstat
  0.00    0.000000           0         1           lstat
  0.00    0.000000           0         9           mmap
  0.00    0.000000           0         3           mprotect
  0.00    0.000000           0         1           munmap
  0.00    0.000000           0         1           brk
  0.00    0.000000           0         1         1 ioctl
  0.00    0.000000           0         1         1 access
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         1           arch_prctl
------ ----------- ----------- --------- --------- ----------------
100.00    0.000013                    78        45 total

### `find`
strace -c find /etc/passwd > /dev/null
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
100.00    0.000043           1        72        60 open
  0.00    0.000000           0        10           read
  0.00    0.000000           0         1           write
  0.00    0.000000           0        14           close
  0.00    0.000000           0        24        18 stat
  0.00    0.000000           0        11           fstat
  0.00    0.000000           0        25           mmap
  0.00    0.000000           0        13           mprotect
  0.00    0.000000           0         4           munmap
  0.00    0.000000           0         3           brk
  0.00    0.000000           0         2           rt_sigaction
  0.00    0.000000           0         1           rt_sigprocmask
  0.00    0.000000           0         3         2 ioctl
  0.00    0.000000           0         1         1 access
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         1           uname
  0.00    0.000000           0         3           fchdir
  0.00    0.000000           0         1           getrlimit
  0.00    0.000000           0         1           statfs
  0.00    0.000000           0         1           arch_prctl
  0.00    0.000000           0         2         1 futex
  0.00    0.000000           0         1           set_tid_address
  0.00    0.000000           0         1           newfstatat
  0.00    0.000000           0         1           set_robust_list
------ ----------- ----------- --------- --------- ----------------
100.00    0.000043                   197        82 total

## Questions

1. Describe the differences you see between the number and type of system calls
   used by your utilities as compared to the standard Unix programs.

The standard Unix programs dd and find use far more system calls than the utilities ./search and ./duplicate.  Specifically, duplicate uses 91, dd uses 160, search uses 78, and find uses 197.  Interestingly, the utilities and their standard original versions uses the same number of stat and access calls. However, dd and find use far more open and close system calls. Alos, dd and find use a wider range of system calls.

2. Did you notice anything surprising about the trace of your utilities or the
   trace of the standard Unix programs? Which implementations are faster or
   more efficient?  Explain.

The utilities performed faster than the standard Unix programs. Speficially, ./duplicate takes about 0.000012 seconds while dd takes about 0.000041 seconds, and ./search takes about 0.000013 seconds while find takes 0.000043 seconds. The improved speed likely occurs because the utilities only implement a subset of all the features included in dd and find. With more open and close system calls, it makes sense that dd and find would perform slower and less efficiently. 

