/* duplicate_options.c: Options Parsing */

#include "duplicate.h"

/* Options Functions */

/**
 * Display usage message and exit.
 * @param   progname        Name of program.
 * @param   status          Exit status.
 */
void        usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [options]\n\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    if=FILE     Read from FILE instead of stdin\n");
    fprintf(stderr, "    of=FILE     Write to FILE instead of stdout\n");
    fprintf(stderr, "    count=N     Copy only N input blocks\n");
    fprintf(stderr, "    bs=BYTES    Read and write up to BYTES bytes at a time\n");
    fprintf(stderr, "    seek=N      Skip N obs-sized blocks at start of output\n");
    fprintf(stderr, "    skip=N      Skip N ibs-sized blocks at start of input\n");
    exit(status);
}

/**
 * Parse command-line options.
 * @param   argc            Number of command-line arguments.
 * @param   argv            Array of command-line arguments.
 * @param   options         Pointer to Options structure.
 * @return  Whether or not parsing the command-line options was successful.
 */
bool        parse_options(int argc, char **argv, Options *options) {
    for (int i = 1; i < argc; i++) {
        // Usage message
        if (streq(argv[i], "-h")) {
            usage(argv[0], 0);
        }
        char *arg = argv[i];
        char *eq = strchr(argv[i], '=');
        // If proper command line argument, parse
        if (eq) {
            char *value = eq;
            value++;
            *eq = '\0'; 
            if (streq(arg, "if")) {
                options->input_file = value;
            }
            else if (streq(arg, "of")) {
                options->output_file = value;
            }
            else if (streq(arg, "count")) {
                options->count = strtol(value, NULL, 10);
            }
            else if (streq(arg, "bs")) {
                options->bytes = strtol(value, NULL, 10);
            }
            else if (streq(arg, "seek")) {
                options->seek = strtol(value, NULL, 10);
            }
            else if (streq(arg, "skip")) {
                options->skip = strtol(value, NULL, 10);
            }
            else {
                usage(argv[0], 1);
            }
        }
        // Usage message if 
        else {
            usage(argv[0], 1);
        }
    }
    return false;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
