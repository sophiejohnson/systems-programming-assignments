/* walk.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/**
 * Run specified program with the bourne shell.
 * @param     command     Command to run.
 * @return    Exit status of child process.
 */

// Usage function
void usage(int status) {
    fprintf(stderr, "Usage: ./doit COMMAND\n");
    exit(status);
}

// Fork process and execute command 
int doit(char *command) {
    int status = 0;
    int result = fork();
    if (result < 0) {
        fprintf(stderr, "Fork failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    else if (result == 0) {
        if (execl("/bin/sh", "sh", "-c", command, (char *) 0) < 0) {
            return EXIT_FAILURE; 
        }
    }
    else {
        wait(&status);
        status = WEXITSTATUS(status);
    }
    return status;
}

int main(int argc, char *argv[]) {

    // Parse command line arguments
    if (argc != 2) {
        usage(EXIT_FAILURE);
    }
    char *command = argv[1];
    
    // Do it
    int status = doit(command);
    return status;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
