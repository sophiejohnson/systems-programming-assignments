/* ncat.c  */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

// Usage statement
void usage(int status) {
    fprintf(stderr, "Usage: ./ncat HOST PORT\n");
    exit(status);
}

int main(int argc, char *argv[]) {

   // Parse command line arguments
   if (argc != 3) {
      usage(EXIT_FAILURE);
   }
   char *host = argv[1];
   char *port = argv[2];

   // Initialize variables
   struct addrinfo *servinfo;
   struct addrinfo  hints = {
      .ai_family   = AF_UNSPEC,
      .ai_socktype = SOCK_STREAM,
   };
   
   // Look up host information
   int status;
   if ((status = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
      fprintf(stderr, "Could not look up %s:%s: %s\n", host, port, gai_strerror(status));
      return EXIT_FAILURE;
   } 

   int sockfd = -1;
   for (struct addrinfo *p = servinfo; p != NULL && sockfd < 0; p = p->ai_next) {
       // Allocate socket
       if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
           fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
           continue;
       }
       // Connect to the host
       if (connect(sockfd, p->ai_addr, p->ai_addrlen) < 0) {
           close(sockfd);
           sockfd = -1;
           continue;
       }
   }

   freeaddrinfo(servinfo);

   // Display error message if failed to connect
   if (sockfd < 0) {
      fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
      return EXIT_FAILURE;
   }
   fprintf(stdout, "Connected to %s:%s\n", host, port);

   // Create stream
   FILE *stream = fdopen(sockfd, "w+");
   if (stream == NULL) {
       fprintf(stderr, "Unable to create stream: %s\n", strerror(errno));
       close(sockfd);
       return EXIT_FAILURE;
   }

   // Connect to stdin
   char buffer[BUFSIZ];
   while (fgets(buffer, BUFSIZ, stdin)) {
      fputs(buffer, stream);
   }

   fclose(stream);
   return EXIT_SUCCESS;
}
