Homework 01

Activity 1: File Operations

| Command                             | Elapsed Time  |
|-------------------------------------|---------------|
| cp -r /usr/share/pixmaps images     | 1.637 seconds |
| mv images pixmaps                   | 0.004 seconds |
| mv pixmaps /tmp/sjohns37-pixmaps    | 0.777 seconds |
| rm -r /tmp/sjohns37-pixmaps         | 0.007 seconds |

1. When using the mv command to rename the folder, the contents of the directory remain in the same place and only the name is changed. However, when moving the pixmaps folder to /tmp, the contents of the folder must be moved into the new directory. This makes the operation take significantly longer. 
2. In a similar way, removing the pixmaps folder from /tmp/ is much faster than the move operation because the contents are deleted from their current directory. Again, using mv requires changing the location of existing files, which takes considerably longer.

Activity 2: Redirection

1. bc < math.txt
2. bc < math.txt > results.txt
3. bc < math.txt > results.txt 2> /dev/null
4. cat math.txt | bc
I/O redirection requires one process (bc), and a pipeline requires two processes (cat and bc). Therefore, using I/O redirection is more efficient.

Activity 3: Pipelines

1. cat /etc/passwd | grep /sbin/nologin | wc -l
2. ps -A | grep bash | wc -l
3. du /etc/* -a 2> /dev/null | sort -n | tail -n 5 
4. who | awk '{print $1}' | sort | uniq | wc -l

Activity 4: Processes & Signals

1a. The command ctrl-c did not work. After pausing TROLL with ctrl-z and finding the PID using the command ps, the commands kill PID, kill -1 PID, and kill -2 PID also failed to terminate the process.

b. After pausing TROLL with ctrl-z and finding the PID using the command ps, the command kill -9 PID terminated the troll.

2a.The pipeline kill -9 $(ps -u $(whoami) | grep TROLL | awk '{print $1}') terminates the process from the second terminal window.

b. The command killall -u $(whoami) -9 TROLL terminates the process from the second terminal window.

3. When using the command kill and the PID of TROLL, the signals -1 and -14 lead to messages including song lyrics. Furthermore, the signal -10 opens a process called cmatrix, and the signal -12 leads to an animated version of Episode IV - A New Hope. 
==========
