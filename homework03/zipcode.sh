#!/bin/sh

# Usage function
usage() {
    cat << EOF
Usage: $(basename $0)

    -c   CITY    Which city to search
    -s   STATE   Which state to search (Indiana)

If no CITY is specified, then all the zip codes for the STATE are displayed.
EOF
    exit $1
}

# Function to find zip codes of all cities
allCities() {
    grep -E http: | grep -Eo '[0-9]{5}' | sort | uniq
}

# Function to find zip code of one city
oneCity() {
    grep -Eio "http:.*($CITY)/.*[0-9]{5}/" | grep -Eo '[0-9]{5}'
}

# Declare variables
URL=http://www.zipcodestogo.com/
STATE=Indiana
whichCITY="allCities"

# Parse command-line options
while [ $# -gt 0 ]; do
    case $1 in
        -c)
            CITY=$2
            whichCITY="oneCity"
            shift
            ;;
        -s)
            STATE=$2
            shift
            ;;
        -h)
            usage 0
            ;;
        *)
            usage 1
            ;;
    esac
    shift
done

# Find zipcode for designated city/cities with filter pipeline
curl -s "$URL$STATE/" | $whichCITY
