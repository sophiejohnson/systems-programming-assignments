#!/bin/sh

# Usage function
usage() { 
    cat << EOF
USAGE: $(basename $0) [rotation]

This program will read from stdin and rotate the text by the specified rotation.  If none is specified, then the default value is 13.
EOF
    exit $1
}

# Function to echo lowercase and uppercase alphabet
source_set() {
    echo "$LOWERCASE$UPPERCASE"
}

# Function to echo shifted alphabets
target_set() {
    newLOWER=$(echo $LOWERCASE | cut -c $(($1 + 1))-)$(echo $LOWERCASE | cut -c -$1)
    newUPPER=$(echo $UPPERCASE | cut -c $(($1 + 1))-)$(echo $UPPERCASE | cut -c -$1)
    echo "$newLOWER$newUPPER"
}

# Parse command-line options
if [ x"$1" = x"-h" ]; then
    usage 0
elif [ $# -eq 0 ]; then
    SHIFT=13
else
    SHIFT=$(($1 % 26))
fi

LOWERCASE=abcdefghijklmnopqrstuvwxyz
UPPERCASE=$(echo $LOWERCASE | tr $LOWERCASE A-Z)

# Filter pipeline to perform encryption
tr $(source_set) $(target_set $SHIFT)
