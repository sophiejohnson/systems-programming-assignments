#!/bin/sh

# Usage function 
usage() {
    cat << EOF
Usage: $(basename $0)

    -d DELIM    Use this as the comment delimiter.
    -W          Don't strip empty lines.
EOF
    exit $1
}

# Set variables
DELIM='#'
noBlanks=";/^[[:space:]]*$/d"

# Parse command-line options
while [ $# -gt 0 ]; do
    case $1 in
        -d)
            DELIM=$2
            shift
            ;;
        -W)
            unset noBlanks
            ;;
        -h) 
            usage 0
            ;;
        *) 
            usage 1
            ;;
    esac
    shift
done

# Filter pipeline to broify code
sed "s|$DELIM.*$||;s|[[:space:]]*$||$noBlanks"
