## Homework 03

### Activity 1: Caesar Cipher

1. I used an if statement to parse the command line arguments. First, if the argument reads "-h", the program displays a usage message. If there aren't any command line arguments, the variable SHIFT is set to 13. Otherwise, assuming the user will input an integer, the variable SHIFT is set to the value entered mod 26. Note, mod 26 allows for shifts greater than 26.

2. Before calling the source set function, the variable LOWERCASE is assigned the letters a-z and the variable UPPERCASE is assigned the letters A-Z by using tr. Then, the source set function echoes the two variables. 

3. The target set function begins by shifting LOWERCASE the appropriate number of times, which is passed as the variable SHIFT ($1). It echoes the value of LOWERCASE and uses cut to find all characters after the indicated number $(($1 + 1))-. Then, it does the same to find all characters up to the indicated number -$1. This process is repeated with the UPPERCASE variable. Finally, the function echoes the two new, shifted variables.

4. To perform the encryption, the tr command is used with the results of the source set function and the results of the target set function respectively. Note, the target set function must be called with the variable SHIFT as an argument.

### Activity 2: Broification

1. To parse the command line arguments, the program uses a while loop which continues until the number of additional arguments is zero. It enters a case structure which responds with the appropriate action for each command: -d) records the deliminator and shifts once, -W) calls a function which modifies a portion of the pipeline, -h) displays a usage message with a success exit code, and \*) displays a usage message with a failure exit code. Then, it shifts and repeats.

2. First, the variable DELIM is set to either the default # or the deliminator specified by the user. The sed command is then used with the regular expression "s|$DELIM.\*$||" to find and delete all instances of a deliminator followed by any characters until the last line.

3. The program assigns the variable noBlanks with the portion of the sed command ";/^[[:space:]]\*$/d", but it unsets the variable if the user uses the -W flag. This regular expression deletes all lines that contain only space characters from the beginning to the end of the line. If the variable exists, the sed command performs the action.

4. As mentioned above, the command line argument -d DELIM indicates the appropriate deliminator to be used. The argument -W unsets the noBlanks variable, which prevents the sed command from removing all blank lines.

### Activity 3: Zip Codes

1. To parse the command line arguments, a while loop continues until there are no longer additional arguments. In the while loop, a case structure performs the appropriate action depending on the argument. The command -c reads in the next argument as the city and designates the proper pipeline to be used, the command -s reads in the next argument as the state, and the -h and default cases display the usage messages with success and failure error codes respectively.  The while loop ends with a shift. Note, the -c and -s commands shift after reading in the next argument.

2. To extract the zipcodes, one of two separate functions is called in the main pipeline depending on whether a user enters a city. If not, the pipeline uses grep to extract all URLs, grepwith the regular expression '[0-9]{5}' to extract the zipcodes, sort, and uniq to find all zipcodes in the state. If the user enters a city, then the pipeline uses grep to find the URL for the city and grep with the same regular expression to extract the zipcode.

3. The main pipeline begins with the command curl on the URL followed by the variable STATE, set to Indiana by default or assigned a string by the user with -s. This provides the correct HTML from which to extract the zipcode. If a user enters a city, then the rest of the pipeline uses grep to find the URLs for the link to each city and then extract the zipcode. This is given by: grep -Eio "http:.\*($CITY)/.\*[0-9]{5}/" | grep -Eo '[0-9]{5}'.

===========
