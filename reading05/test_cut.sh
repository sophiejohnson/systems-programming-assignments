#!/bin/bash

SCRIPT=cut.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing $SCRIPT ..."

printf " %-40s ... " "Usage"
./$SCRIPT -h 2>&1 | grep -i usage 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    error "Failed Usage Test"
else
    echo "Success"
fi

for i in $(seq 1 6); do
    printf " %-40s ... " "cut -d : -f $i   on /etc/passwd"
    ./$SCRIPT -d : -f $i /etc/passwd | diff -y - <(cut -d : -f $i /etc/passwd) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
	echo "Success"
    fi
    printf " %-40s ... " "cut -d : -f 1,$i on /etc/passwd"
    ./$SCRIPT -d : -f 1,$i /etc/passwd | diff -y - <(cut -d : -f 1,$i /etc/passwd) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
	echo "Success"
    fi
    printf " %-40s ... " "cut -d : -f $i,1 on /etc/passwd"
    ./$SCRIPT -d : -f $i,1 /etc/passwd | diff -y - <(cut -d : -f $i,1 /etc/passwd) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
	echo "Success"
    fi
    printf " %-40s ... " "cut -d : -f $i,$i on /etc/passwd"
    ./$SCRIPT -d : -f $i,$i /etc/passwd | diff -y - <(cut -d : -f $i,$i /etc/passwd) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
	echo "Success"
    fi
done
    
printf " %-40s ... " "cut -d : -f 1   on stdin (implicit)"
cat /etc/passwd | ./$SCRIPT -d : -f 1 | diff -y - <(cat /etc/passwd | cut -d : -f 1) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "cut -d : -f 1   on stdin (explicit)"
cat /etc/passwd | ./$SCRIPT -d : -f 1 - | diff -y - <(cat /etc/passwd | cut -d : -f 1 -) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

echo "   Score $(echo "scale=2; (27 - $FAILURES) / 27.0 * 1.5" | bc)"
echo
