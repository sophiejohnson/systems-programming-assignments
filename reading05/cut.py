#!/usr/bin/env python3

import os
import sys

# Global variables
delim = '\t'
fields = 1

# Usage function
def usage(status = 0):
   print('''Usage: {} files...

   -d  DELIM   use DELIM instead of TAB for field delimiter
   -f  FIELDS  select only these fields
   '''.format(os.path.basename(sys.argv[0])))
   sys.exit(status)

# Parse command line options
args = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
   arg = args.pop(0)
   # Read in deliminator
   if arg == '-d':
      delim = args.pop(0)
  # Read in and sort uniq fields 
   elif arg == '-f':
      fields = args.pop(0).split(',')
      uniqFields = set()
      for field in fields:
         uniqFields.add(field)
      fields[:] = []
      for uniqField in uniqFields:
          fields.append(uniqField)
      fields = sorted(fields)
   # Display usage function 
   elif arg == '-h':
      usage(0)
   else:
      usage(1)

if not args:
   args.append('-')

# Main execution
for path in args:
   stream = sys.stdin if path == '-' else open(path)

# Display designated fields of each line
for line in stream:
   line = line.rstrip()
   splitLine = line.split(delim)
   finalLine = []
   for field in fields:
      finalLine.append(splitLine[int(field) - 1])
      if field == fields[-1]:
         finalLine.append('\n')
      else:
         finalLine.append(delim)
   for n in finalLine:
      print(n, end = '')
