#!/usr/bin/env python3

import os
import sys

# Global variables
dispCount = False
uniq = {}

# Usage function
def usage(status = 0):
   print('''Usage: {} files...

   -c prefix lines by the number of occurrences
   '''.format(os.path.basename(sys.argv[0])))
   sys.exit(status)

# Parse command line options
args = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
   arg = args.pop(0)
   # Update boolean if need to count
   if arg == '-c':
      dispCount = True
   # Display usage function 
   elif arg == '-h':
      usage(0)
   else:
      usage(1)

if not args:
   args.append('-')

# Main execution
for path in args:
   stream = sys.stdin if path == '-' else open(path)

# Add name to list, update number of instances
for line in stream:
   line = line.rstrip()
   if uniq.get(line) == None:
      uniq[line] = 1
   else:
      uniq[line] = uniq[line] + 1

# Display results
if dispCount:
   for name, num in uniq.items():
       print('{:>7} {}'.format(num, name))
else:
   for name, num in uniq.items():
      print(name)
