#!/bin/bash

SCRIPT=uniq.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing $SCRIPT ..."

printf " %-40s ... " "Usage"
./$SCRIPT -h 2>&1 | grep -i usage 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    error "Failed Usage Test"
else
    echo "Success"
fi

printf " %-40s ... " "uniq    on /etc/group"
cut -d : -f 4 /etc/group | sort | ./$SCRIPT | sort | diff -y - <(cut -d : -f 4 /etc/group | sort | uniq | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-40s ... " "uniq -c on /etc/group"
cut -d : -f 4 /etc/group | sort | ./$SCRIPT -c | sort | diff -y - <(cut -d : -f 4 /etc/group | sort | uniq -c | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "uniq    on /etc/passwd"
cut -d : -f 7 /etc/passwd | sort | ./$SCRIPT | sort | diff -y - <(cut -d : -f 7 /etc/passwd | sort | uniq | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-40s ... " "uniq -c on /etc/passwd"
cut -d : -f 7 /etc/passwd | sort | ./$SCRIPT -c | sort | diff -y - <(cut -d : -f 7 /etc/passwd | sort | uniq -c | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "uniq    on /etc/fstab"
awk '{print $3}' /etc/fstab | sort | ./$SCRIPT | sort | diff -y - <(awk '{print $3}' /etc/fstab | sort | uniq | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-40s ... " "uniq -c on /etc/fstab"
awk '{print $3}' /etc/fstab | sort | ./$SCRIPT -c | sort | diff -y - <(awk '{print $3}' /etc/fstab | sort | uniq -c | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "uniq    on stdin (explicit)"
cut -d : -f 7 /etc/passwd | sort | ./$SCRIPT - | sort | diff -y - <(cut -d : -f 7 /etc/passwd | sort | uniq - | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-40s ... " "uniq -c on stdin (explicit)"
cut -d : -f 7 /etc/passwd | sort | ./$SCRIPT -c - | sort | diff -y - <(cut -d : -f 7 /etc/passwd | sort | uniq -c - | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

echo "   Score $(echo "scale=2; (9 - $FAILURES) / 9.0 * 1.5" | bc)"
echo
