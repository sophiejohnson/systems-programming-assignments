#!/usr/bin/env python3

import os
import sys

# Global variable
sortReverse = False

# Usage function
def usage(status = 0):
   print('''Usage: {} files...)

   -r reverse the result of comparisons
   '''.format(os.path.basename(sys.argv[0])))
   sys.exit(status)

# Parse command line options
args = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
   arg = args.pop(0)
   if arg == '-r':
      sortReverse = True
   elif arg == 'h':
      usage(0)
   else:
      usage(1)

if not args:
   args.append('-')

# Connect to file
for path in args:
   stream = sys.stdin if path == '-' else open(path)

itemList = []

# Create list with each line
for line in stream:
   line = line.rstrip()
   itemList.append(line)

# Sort list
if sortReverse:
   itemList = sorted(itemList, reverse = True)
else:
   itemList = sorted(itemList)

# Print list
for item in itemList:
   print(item)
