#!/bin/sh

# Functions

usage() {
    cat <<EOF
Usage: $(basename $0) [-n NUMBER -p PATTERN -s]

    -n NUMBER	Number of items to output (default is 10).
    -p PATTERN	Pattern to match.
    -s		Output items in sorted order.

Extracts up to the first NUMBER items from STDIN that match specified PATTERN.
EOF
    exit $1
}

# Parse command-line options

NUMBER=10
PATTERN=""
SORT=0

while [ $# -gt 0 ]; do
    case $1 in
        -n)
            NUMBER=$2
            shift;;
        -p)
            PATTERN=$2
            shift;;
        -s) 
            SORT=1;;
        -h)
            usage 0;;
    esac
    shift
done

if [ -z $PATTERN ]; then
    usage 1
fi

# Main execution

if [ $SORT -eq 1 ]; then
    grep -o $PATTERN | sort | head -n $NUMBER
else
    grep -o $PATTERN | head -n $NUMBER
fi
