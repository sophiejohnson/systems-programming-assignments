#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "sys/stat.h"

int main() {
   // Create variables
   DIR *dir = opendir("./");
   struct dirent *file;
   struct stat info;
   while ((file = readdir(dir)) != NULL) {
       // Check if each file exists
       if (stat(file->d_name, &info) == 0 && S_ISREG(info.st_mode)) {
           // Print name and size
           printf("%s %d\n", file->d_name, (int)info.st_size);
       }
   }
   closedir(dir);
}
