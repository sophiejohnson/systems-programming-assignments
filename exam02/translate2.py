#!/usr/bin/env python3

import requests

url = 'http://yld.me/raw/lmz'

lines = (line.strip().split(',') for line in requests.get(url).text.splitlines())
lines = (line[0] for line in sorted(lines, key = lambda a: a[3]))
for line in lines:
   print(line.upper())
