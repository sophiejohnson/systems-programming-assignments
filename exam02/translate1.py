#!/usr/bin/env python3

lines = [line.rstrip().split(':')[6] for line in open('/etc/passwd')]
print(len(set(lines)))
