/* str_title.c: convert strings to title-case */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Copy string and convert to title case
char * str_title(const char *s) {
   char *t = calloc(strlen(s) + 1, sizeof(char));
   strncpy(t, s, strlen(s) + 1);
   char *c = t;
   while (*c) {
      *c = toupper(*c);
      while (*c && !isspace(*++c));
      while (*c &&  isspace(*++c));
   }
   return t;
}

// Main execution
int main(int argc, char *argv[]) {
    // Call function with each command line argument
    for (int i = 1; i < argc; i++) {
      char *t = str_title(argv[i]);
      puts(t);
      free(t);
   }
   return 0;
}
