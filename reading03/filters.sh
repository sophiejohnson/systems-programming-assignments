#!/bin/bash

q1_answer() {
    echo "All your base are belong to us" | tr a-z A-Z
}

q2_answer() {
    echo "monkeys love bananas" | sed 's/monkeys/gorillaz/' 
}

q3_answer() {
    echo "     monkeys love bananas" | sed 's/^[ \t]*//' 
}

q4_answer() {
    curl -sL https://yld.me/raw/yWh | sort -t ':' -k 3 | head -n 1 | awk -F : '{print $7}'
}

q5_answer() {
    curl -sL https://yld.me/raw/yWh | sed -E 's_\/bin\/(bash|csh|tcsh)$_\/usr\/bin\/python_' | grep python 
}

q6_answer() {
    curl -sL https://yld.me/raw/yWh | grep -E ':4[[:digit:]]*7:' 
}
