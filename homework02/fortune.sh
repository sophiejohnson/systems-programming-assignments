#!/bin/sh

# Add cowsay to path
export PATH=/afs/nd.edu/user15/pbui/pub/bin:$PATH

# Respond to exit signals
trap cleanup 1 2 15

cleanup(){
    echo $'\n'
    (shuf | head -n 1 | cowsay) <<- EOF 
        Well, I guess this is goodbye!
        So long, my friend!
        That was pretty fun. I'll see you next time!
        I'll miss you dearly. Come back soon!
EOF
    # Exit with an error code
    exit 1
}

# Greet user
cowsay "Hello $USER!"

# Prompt until nonempty answer
question=""
while [[ -z "${question// }" ]] 
do
    read -p "Question? " question
done

# Display random answer
(shuf | head -n 1 | cowsay) <<- EOF
    It is certain
    It is decidedly so
    Without a doubt
    Yes, definitely
    You may rely on it
    As I see it, yes
    Most likely
    Outlook good
    Yes
    Signs point to yes
    Reply hazy try again
    Ask again later
    Better not tell you now
    Cannot predict now
    Concentrate and ask again
    Don't count on it
    My reply is no
    My sources say no
    Outlook not so good
    Very doubtful
EOF
