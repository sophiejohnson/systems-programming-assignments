Homework 02

Activity 1: Saving the World

1. I began the script with an if statement which checks if the number of additional arguments ($#) is equal to one. If true, then a usage message is displayed. If false, the script considers each argument.

2. If there are additional arguments, the script enters a for loop. The for loop uses the variable "archive" which takes on the value of each of the arguments ($@). Then, a case statement and wildcards determine the appropriate command to use for each argument. Note, each case includes two different extensions which can be extracted in the same way.

3. The most challenging aspect of writing the script was using proper syntax. However, with reference to "The Linux Shell Scripting Tutorial", I was able to determine the proper way to implement an if statement, for loop, and case statement.

Activity 2: Predicting the Future

1. To display random messages to the user, I used a here document to include all possible messages. Then, I created a pipeline (shuf | head -n 1 | cowsay) which shuffles the input lines, isolates the first line, and then displays the selected line with cowsay. By using the here document as input to the pipeline, I was able to display the random messages.

2. To respond to signals, I used a trap function to identify SIGHUP (1), SIGINT (2), or SIGTERM (15) signals. In response to these signals, a cleanup function is called to display a random goodbye message to the user and then exit with an error code (exit 1). 

3. After creating an empty variable called "question", the script enters a while loop which continues as long as the input line is empty. Then, the line "read -p "Question? " question" prompts the user for a question, allows the user to type on the same line, and records the response in the variable "question". This continues until a nonempty answer is recorded.

4. The most challenging aspect of writing the fortune.sh script was figuring out how to display a random message using a here document. While I understood the concept of consolidating one large body of text, I was not sure how to then use the data. After trying and failing the assign the sayings to a variable, I realized that the here documents should be redirected into a command. I then researched shuf to figure out how I could construct my pipeline.

Activity 3: Meeting the Oracle

1. First, I scanned "xavier.h4x0r.space" for an HTTP port between 9000 and 9999.

Command:
    nmap -Pn -p 9000-9999 xavier.h4x0r.space
Output:
    Starting Nmap 5.51 ( http://nmap.org ) at 2018-02-02 15:06 EST
    Nmap scan report for xavier.h4x0r.space (129.74.160.130)
    Host is up (0.00030s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE
    9104/tcp open  jetdirect
    9206/tcp open  unknown
    9522/tcp open  unknown

The output showed that there are three open ports from 9000-9999.

2. Next, I tried to access the HTTP server.

Command:
    curl xavier.h4x0r.space:9104
Output:
    ASCII Image with Message

The message told me to come back with a password and request the doorman. It instructed me to find my lockbox in ~pbui/pub/oracle/lockboxes and retrieve my passcode using brute force.

3. To find my lockbox, I searched the contents of the given directory and used a pipeline with grep to find files including my netid.

Command:
    find ~pbui/pub/oracle/lockboxes | grep sjohns37
Output:
    /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/69d21a84/70c28d3b/1bd56e6a/6c65b32a/1a51ea12/sjohns37.lockbox

The output gave me the location of my lockbox.

4. I then switched to the location of my lockbox and attempted to run the program.

Command:
    cd /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/69d21a84/70c28d3b/1bd56e6a/6c65b32a/1a51ea12/
    ./sjohns37.lockbox
Output:
    Usage: lockbox password

The output message showed me the appropriate usage of the lockbox program. It must be entered with an additional command line argument as the password.

5. To generate possible passwords from the file itself, I used a for loop and the strings command. I tested each string found as a potential password.

Command:
    for string in $(strings ./sjohns37.lockbox); do ./sjohns37.lockbox $string; done
Output:
    e29f9070c89d4abd11ac9c20166741e21570eb0e

Because one of the strings worked as the password, my passcode was revealed.

6. Once I had my passcode, I returned to the original port with my username and passcode as instructed.

Command:
    curl xavier.h4x0r.space:9104/sjohns37/e29f9070c89d4abd11ac9c20166741e21570eb0e
Output:
    ASCII Image with Message

The new message directed me toward Bobbit on Slack. I was told to send him a direct message to verify my netid and recently acquired passcode. Then, the instructions said to go to port 9522 to deliver the message to the Oracle.

7. So, I went to slack and sent Bobbit a direct message.
Command:
    !verify sjohns37 e29f9070c89d4abd11ac9c20166741e21570eb0e
Output:
    Hi sjohns37! Please tell the ORACLE the following MESSAGE: ZndidWFmMzc9MTUxNzYwODExOQ==

Now, I finally had the message to give to the Oracle.

8. As instructed, I connected to port 9522 of the machine.
Command:
    nc xavier.h4x0r.space 9522
Output:
    ASCII Image with Message

The message prompted me for my name.

9. I then entered my name.

Command: 
    sjohns37
Output: 
    ASCII Image with Message

I was asked what message I brought for the Oracle.

10. I entered the message given to me by Bobbit.

Command:
    ZndidWFmMzc9MTUxNzYwODExOQ==
Output: 
    ASCII Image with Message

The message asked me why it took so long to find the Oracle after speaking with Bobbit.

11. Finally, I admitted by greatest weakness.

Command: 
    Trying to learn vim is a nightmare.
Output: 
    Congratulations sjohns37! You have reached the end of this journey.
    I hope you learned something from the ORACLE :]

I certainly learned a lot from the Oracle. In addition to learning how to scan a machine for open ports, I figured out how to make connections to those open ports. I saw the difference between using wget and curl. I learned how the strings command works and how to redirect output to be used with another command when searching for my passcode. Finally, I definitely learned how to effectively consult man pages, use --help for commands, and try different approaches until something works. 

=========
