#!/bin/sh

if [ $# -eq 0 ]; then
    echo "Usage: $0 archive1 archive2..."
else
    for archive in $@
    do
        case $archive in
            *.tgz | *.tar.gz)
                tar xzvf $archive
                ;;
            *.tbz | *.tar.bz2)
                tar xjvf $archive
                ;;
            *.txz | *.tar.xz)
                tar xJvf $archive 
                ;;
            *.zip | *.jar)
                unzip $archive
                ;;
            *)
                echo "Unknown archive format: $archive"
        esac
    done    
fi
