Homework 06
===========

## Activity 1 - Questions

#### 1. 
What is the difference between a shared library such as `libstr.so` and a
   static library such as `libstr.a`?

A shared library includes functions which are accessed externally by executables when they are run. Specifically, shared libraries allow other programs to search for the needed functions and access specific pieces of code upon run time. A static library also contains all the code for the library, but it does not need to be accessed by the executables at runtime. Instead, it is pasted directly into other programs in order to be utilized.

#### 2. 
Compare the sizes of `libstr.so` and `libstr.a`, which one is larger? Why?

The size of `libstr.so` is 12545, and the size of `libstr.a` is 10410. The dynamic library `libstr.so` is larger because it needs to be accessible by executables at runtime. The more modular design which supports connection to each executable requires more space. Contrastingly, the static library can be stored more compactly because it does not need to be accessed by other programs.

## Activity 2 - Questions

#### 1.
What is the difference between a static executable such as `str-static`
   and a dynamic executable such as `str-dynamic`?

A static executable copies the entire library and includes it in the executable. As a result, the binary runs independent of the library itself. In contrast, a dynamic executable references the code in the library at run-time. The library's code is not inserted into the main code when compiled; instead, the executable searches the LD_LIBRARY_PATH for the library and includes the appropropriate code as needed. This reduces the amount of code replicated.

#### 2. 
Compare the sizes of `str-static` and `str-dynamic`, which one is larger?
   Why?

The size of `str-static` is 907727, which is significantly larger than `str-dynamic`'s size 14301. As expected, the static executable is much larger because it includes the entire library in its binary. The dynamic executable does not include any of the library's code because it references the library each time it is run.

#### 3. 
Login into a new shell and try to execute `str-dynamic`.  Why doesn't
   `str-dynamic` work?  Explain what you had to do in order for `str-dynamic`
   to actually run.

The shell cannot execute `str-dynamic` because it cannot find the shared library `libstr  .so` in the LD_LIBRARY_PATH. To run the dynamic executable, the current directory must be added to the path with the command:
   export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
Now, it is possible to locate the dynamic library and access the needed functions.

#### 4. 
Login into a new shell and try to execute `str-static`.  Why does
   `str-static` work, but `str-dynamic` does not in a brand new shell session?

The shell can execute `str-static` because the library's code is already included in the binary. It runs independent of the library itself so there is no need to search the path of libraries.
