/* library.c: string utilities library */

#include "str.h"

#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	str_lower(char *s) {
    for(char *c = s; *c; c++) {
        *c = tolower(*c);
    }
    return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	str_upper(char *s) {
    for(char *c = s; *c; c++) {
        *c = toupper(*c);
    }
   return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	str_startswith(const char *s, const char *t) {
    bool startsWith = (strlen(s) >= strlen(t))? true : false;
    while(*t && startsWith) {
        if(*(s++) != *(t++)) {
            startsWith = false;
        }
    }
    return startsWith;
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	str_endswith(const char *s, const char *t) {
    bool endsWith = (strlen(s) >= strlen(t))? true : false;
    s = s + strlen(s) - strlen(t);
    while(*t && endsWith) {
        if(*(s++) != *(t++)) {
            endsWith = false;
        }
    }
    return endsWith;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	str_chomp(char *s) {
    char *c = s + strlen(s) - 1;
    if(*c == '\n') {
        *c = '\0';
    }
    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	str_strip(char *s) {
    char *read = s;
    char *write = s;
    // Rewrite strings without spaces at beginning of string
    while(*read && isspace(*read)) {
        read++;
    }
    while(*read) {
    *(write++) = *(read++);
    }
    // Record null character and move write pointer to end
    *write = *read;
    write = read;
    // Remove spaces from end of string
    char *end = s + strlen(s) - 1;
    while(*end && isspace(*end)) {
        *(end--) = '\0';     
    }
    return s;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	str_reverse(char *s) {
    char *begin = s;
    char *end = s + strlen(s) - 1;
    while(begin < end) {
        char temp = *begin;
        *(begin++) = *end;
        *(end--) = temp;
    } 
    return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	str_translate(char *s, char *from, char *to) {
    char translate[128] = {}; 
    // If source and target are equal lengths, translate
    if(strlen(from) == strlen(to)) {
        // Create lookup table
        while(*from) {
            translate[(int)*(from++)] = *(to++);
        }
        // Make replacements in string
        for(char *c = s; *c; c++) {
            if(translate[(int)*c]) {
                *c = translate[(int)*c];
            }   
        }
    }
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	str_to_int(const char *s, int base) {
    int finalNum = 0, charNum = 0, baseNum = 1;
    // Traverse string starting at end
    for(char *end = (char *)s + strlen(s) - 1; end >= s; end--) {
        charNum = *end;
        // Fing value of digits
        if(isdigit(charNum)) {
            charNum -= '0';
        }
        // Find value of letters
        else if(isalpha(charNum) && base == 16) {
            charNum = toupper(charNum) - 'A' + 10;
        }
        // Convert to decimal
        finalNum += charNum * baseNum;
        baseNum *= base;
    }
    return finalNum;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
