/* main.c: string library utility */

#include "str.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Modes */

enum {
    STRIP = 1<<1,
    REVERSE = 1<<2,
    LOWER = 1<<3,
    UPPER = 1<<4,
    START = 1<<5,
    END = 1<<6
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s          Strip whitespace\n");
    fprintf(stderr, "   -r          Reverse line\n");
    fprintf(stderr, "   -l          Convert to lowercase\n");
    fprintf(stderr, "   -u          Convert to uppercase\n");
    fprintf(stderr, "   -t FILTER   Only include lines that start with FILTER\n");
    fprintf(stderr, "   -d FILTER   Only include lines that end with FILTER\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, char *filter, int mode) {
    /* Process each line in stream by performing transformations */
    char buffer[BUFSIZ];
    while(fgets(buffer, BUFSIZ, stream)) {
        bool disp = true;
        // Remove newline character
        str_chomp(buffer);
        // Perform transformations
        if(source != NULL && target != NULL) 
            str_translate(buffer, source, target); 
        if(mode & STRIP) 
            str_strip(buffer); 
        if(mode & REVERSE) 
            str_reverse(buffer);
        if(mode & LOWER) 
            str_lower(buffer);
        if(mode & UPPER) 
            str_upper(buffer);
        // Determine whether line starts/ends with filter
        if(mode & START && !str_startswith(buffer, filter))
            disp = false;
        if(mode & END && !str_endswith(buffer, filter))
            disp = false;
        // Display results
        if(disp) {
            printf("%s\n", buffer);
        }
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int argIndex = 1, mode = 0;
    char *filter = NULL, *source = NULL, *target = NULL;
    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    while(argIndex < argc && strlen(argv[argIndex]) > 1 && argv[argIndex][0] == '-') {
        char *arg = argv[argIndex++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 's':
                mode |= STRIP;
                break;
            case 'r':
                mode |= REVERSE;
                break;
            case 'l':
                mode |= LOWER;
                break;
            case 'u':
                mode |= UPPER;
                break;
            case 't':
                mode |= START;
                arg = argv[argIndex++];
                filter = arg;
                break;
            case 'd':
                mode |= END;
                arg = argv[argIndex++];
                filter = arg;
                break;
            default:
                usage(1);
        }
    } 
    // Record source and target strings
    if (argIndex != argc) {
        source = argv[argIndex++];
        target = argv[argIndex++];
    }
    
    /* Translate stream */
    translate_stream(stdin, source, target, filter, mode);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
