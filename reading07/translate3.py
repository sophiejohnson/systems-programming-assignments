#!/usr/bin/env python3

import re
import requests

# Load text file into list with each line
lines = requests.get('http://yld.me/raw/lmz').text.splitlines()
# Separate field 1
names = (line.split(',')[1] for line in lines)
# Find names start with b
bNames = [x for x in names if re.search(r'^B.*', x)]
# Print results
for name in sorted(bNames):
    print(name) 

