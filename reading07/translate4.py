#!/usr/bin/env python3

import os

# Read in lines of file
files = os.popen('/bin/ls -l /etc').read().splitlines()
# Separate field
field2 = (int(x.split()[1]) for x in files)
# Sort and count frequency
nums = {}
for x in sorted(field2):
   nums[x] = 1 if x not in nums else nums[x] + 1
# Print results
for num, freq in nums.items():
    print('{:7d} {}'.format(freq, num))
