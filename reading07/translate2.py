#!/usr/bin/env python3

import re

# All lines of file
fields = open('/etc/passwd').read().splitlines()
# 5th field of each line
fields = [field.split(':')[4] for field in fields]
# Length of fields with pattern
print(len([field for field in fields if re.findall(r'[Uu]ser', field)])) 
